﻿using System;

namespace PatronSingletonDanny
{
    class Program
    {
        static void Main(string[] args)
        {
            //Obtengo la instancia creada por primera vez

            Singleton uno = Singleton.ObtenerInstancia();

            //Relleno datos al azar de la instancia
            uno.RellenoDatos("Diego", 30);
            uno.ProcesoVariado();
            Console.WriteLine(uno);
            Console.WriteLine("---------");

            //Obtengo la instancia

            Singleton dos = Singleton.ObtenerInstancia();

            //Aqui imprimo y comprobamos si es la misma instancia y si lo es tendra el mismo estado

            Console.WriteLine(dos);
        }
    }
}
