﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronSingletonDanny
{
    public class Singleton
    {
        //Aqui guardo la unica instancia que va a existir
        private static Singleton unicains;

        //Propiedades propias de la clase
        public string Nombre { get; set; }
        private int Edad { get; set; }

        //Creo un constructor de acceso privado para impedir acceso a instancia
        private Singleton()
        {
            Nombre = "Sin asingar";
            Edad = 20;
        }

        //Creo este metodo para obtener instancia, con este metodo verifico que solo tengo una instancia, a la que soy accesible o no.

        public static Singleton ObtenerInstancia()
        {
            //Verifico si no existe la instancia
            if (unicains == null)
            {
                //Si no existe, la instanciara automaticamente..por que esta referencia a este objeto de tipo singleton
                unicains = new Singleton();

                Console.WriteLine("La instancia fue creada por primera vez");
            
            }

            //Retornamos a la instancia
            return unicains;

        }
        public override string ToString()
        {
            return string.Format( "La persona {0} , tiene edad de {1}", Nombre , Edad  ); 
        }
        public void RellenoDatos(String pnombre, int pedad)
        {
            Nombre = pnombre;
            Edad = pedad;
        }
        public void ProcesoVariado()
        {
            Console.WriteLine("{0} esta estudiando algo", Nombre);
        }
    }

}
